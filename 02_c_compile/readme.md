# Assembly Example
## Goals
In this example we see how 
1. how to call a C function from the startup file
2. how the a C function is compiled into machine code

## Setup
Same as example #1

## Compiler
The C compiler translates C code into machine code.
The command "arm-none-eabi-gcc" performs this for us. The flag -c instructs gcc to only compile the file,
withour linking. -g adds debug information into the build.
-I adds an include directory. 

