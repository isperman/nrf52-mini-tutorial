#include "twice.h"

int twice(int someValue)
{
    return someValue * 2;
}

float twice_f(float someValue)
{
    return someValue * 2;
}
