#include "twice.h"
#include "nrf52.h"

/**
 * Init the floating point unit according to "Example 4-1 Enabling the FPU"
 * of Cortex-M4 Devices Generic User Guide (DUI 0553B)
*/
void init_FPU() {
  // taken from system_nrf52.c
  //SCB->CPACR |= (3UL << 20) | (3UL << 22);
  int* CPACR_a;
  CPACR_a = (int*) 0xE000ED88;
  *CPACR_a |=  (3UL << 20) | (3UL << 22);
  // Address of CPACR is 0xE000ED88
  // Integrating assembly instruction in our C code
  __asm("dsb sy");
  __asm("isb sy");
}

int  main(void) {
  /* since we use floats and we compile the source to use hard floats,
  * the floating point unit FPU must be activated, otherwise an Hard Fault will be generated.
  */
  init_FPU();
  int myInt = 4;
  float myFloat = 0.003;

  float otherFloat = twice_f(myFloat);
  int otherInt = twice(myInt);

  return 0;
}
